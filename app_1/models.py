from django.db import models

# Create your models here.
class Message(models.Model):
	dari = models.CharField(max_length=27)
	untuk = models.CharField(max_length=27)
	pesan = models.CharField(max_length=40)
	created_date = models.DateTimeField(auto_now_add=True)