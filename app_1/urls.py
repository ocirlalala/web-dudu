from django.conf.urls import url
from .views import index, add_msg

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add_msg', add_msg, name='add_msg'),
]