from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_msg
from .models import Message
from .forms import Message_Form

# Create your tests here.
class App1UnitTest(TestCase):

	def test_app_1_url_is_exist(self):
		response = Client().get('/app-1/')
		self.assertEqual(response.status_code, 200)

	def test_app_1_using_index_func(self):
		found = resolve('/app-1/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_msg(self):
		new_msg = Message.objects.create(dari='Rico', untuk='Amma', pesan='Ngopi apa ngopi?')
		count_all_available_msg = Message.objects.all().count()
		self.assertEqual(count_all_available_msg, 1)

	def test_form_validation_for_blank_items(self):
		form = Message_Form(data={'dari': '', 'untuk': '', 'pesan': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['dari'],
			['This field is required.']
		)

	def test_app_1_post_success_and_render_the_result(self):
		test = 'test'
		response_post = Client().post('/app-1/add_msg', {'dari': test, 'untuk': test, 'pesan': test})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/app-1/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_app_1_post_error_and_render_the_result(self):
		test = 'test'
		response_post = Client().post('/app-1/add_msg', {'dari': '', 'untuk': '', 'pesan': ''})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/app-1/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)