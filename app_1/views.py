from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
response = {}

def index(request):
	msg_list = Message.objects.all()[::-1]
	response['msg_list'] = msg_list
	response['msg_form'] = Message_Form
	html = 'app_1/index.html'

	page = request.GET.get('page', 1)
	paginator = Paginator(msg_list, 4)

	try:
		msg_list_data = paginator.page(page)
	except PageNotAnInteger:
		msg_list_data = paginator.page(1)
	except EmptyPage:
		msg_list_data = paginator.page(paginator.num_pages)
	response['msg_list_data'] = msg_list_data

	return render(request, html, response)

def add_msg(request):
	form = Message_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['dari'] = request.POST['dari']
		response['untuk'] = request.POST['untuk']
		response['pesan'] = request.POST['pesan']
		msg = Message.objects.create(dari=response['dari'], untuk=response['untuk'], pesan=response['pesan'])
		msg.save()
		html = 'app_1/index.html'
		render(request, html, response)
		return HttpResponseRedirect('/app-1/')
	else:
		return HttpResponseRedirect('/app-1/')