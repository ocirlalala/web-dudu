from django import forms

class Message_Form(forms.Form):
	error_messages = {
		'required': 'Tolong isi input ini',
	}
	dari_attr = {
		'type': 'text',
		'class': 'form-input',
	}
	untuk_attr = {
		'type': 'text',
		'class': 'form-input',
	}
	pesan_attr = {
		'type': 'text',
		'class': 'form-input',
	}

	dari = forms.CharField(label='Dari :', required=True, max_length=27, widget=forms.TextInput(attrs=dari_attr))
	untuk = forms.CharField(label='Untuk :', required=True, max_length=27, widget=forms.TextInput(attrs=untuk_attr))
	pesan = forms.CharField(label='Pesan :', required=True, max_length=40, widget=forms.TextInput(attrs=pesan_attr))